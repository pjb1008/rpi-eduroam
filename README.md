# rpi-eduroam

Configure a Raspberry Pi to use Eduroam.

This is for users based in University of Cambridge only.
Visitors from other institutions should use their home institution's instructions.

## Getting a token

Go to [Eduroam Tokens](https://tokens.csx.cam.ac.uk) and create a username and token.

## Downloading the script

Either git clone the repository, or get the just the script using:

```
wget https://gitlab.developers.cam.ac.uk/pjb1008/rpi-eduroam/-/raw/main/rpi-install-eduroam
```

## Installing the token

Run the script 

```sudo bash rpi-install-eduroam```

Enter the username (including the @cam.ac.uk) and the token when prompted.

If all goes well, you will be connected to the Eduroam network when the script exits.

## Notes

Other network configurations will be removed! After running, the only configured network will be Eduroam.

The wireless driver order will be swapped from the normal configuration to work around a problem with the default driver.

The UIS CA certificate is used (the one corresponding to _token-public@cam.ac.uk). 
The CA is trusted for wireless authentication only.
